import { Component, OnInit } from '@angular/core';
import { AxiosResponse } from 'axios';
import * as _ from 'lodash';
import { DateTime } from 'luxon';

import { LookupService } from '../../../shared/lookup-service';
import { PatientListService } from './patient-list.service';
import { ActivatedRoute, Router } from '@angular/router';

import { NgxSpinnerService } from 'ngx-spinner';
import { Message, ConfirmationService, MessageService } from 'primeng/api';
import { Table } from 'primeng/table';
import { MenuItem } from 'primeng/api';

@Component({
    selector: 'app-patient-list',
    templateUrl: './patient-list.component.html',
    styleUrls: ['./patient-list.component.scss'],
    providers: [MessageService, ConfirmationService],
})
export class PatientListComponent implements OnInit {
    admitId: any;
    doctorID: any;
    dataWard: any;
    wardName: any;
    wards: any = [];
    query: any = '';
    dataSet: any[] = [];
    loading = false;
    doctors: any = [];

    total = 0;
    pageSize = 20;
    pageIndex = 1;
    offset = 0;
    user_login_name: any;
    isVisible = false;
    isOkLoading = false;
    inputValue?: string;
    modalTitle: string = 'ระบุแพทย์เจ้าของไข้';
    selectedValue: any = null;

    queryParamsData: any;
   
    selectedWard:any =[];
    date2: Date | undefined;
    activeItem:any;
    nodes:any=
        [
            {
                key: '0',
                label: 'Documents',
                data: 'Documents Folder',
                icon: 'pi pi-fw pi-inbox',
            },
            {
                key: '0',
                label: 'Documents',
                data: 'Documents Folder',
                icon: 'pi pi-fw pi-inbox',
            }
    
            ];
            visibleChangDoctor: boolean = false;
            
           items: MenuItem[]  = [
                {
                    label: '',
                    icon: 'pi pi-fw pi-file',
                    items: [
                        {
                            label: 'ย้ายตึก/เตียง',
                            icon: 'pi pi-fw pi-file-import',
                            routerLink:'/change-ward'
                           
                        },
                        {
                            label: 'ระบุแพทย์',
                            icon: 'pi pi-fw pi-user-plus',
                            command: (e) => {
                                console.log(this.activeItem);
                                this.visibleChangDoctor = true;

                                // this.deleteAdmit(this.activeItem);
                              }

                        },
                        {
                            separator: true
                        },
                        {
                            label: 'ลบรายการ',
                            icon: 'pi pi-fw pi-trash',
                            command: (e) => {
                                console.log(this.activeItem);
                                this.deleteAdmit(this.activeItem);
                              }
                           
                        }
                    ]
                },
               
            ];

    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,

        private patientListService: PatientListService,

        private lookupService: LookupService,
        private messageService: MessageService,
        private confirmationService: ConfirmationService,
        private spinner: NgxSpinnerService
    ) {
        let jsonString: any = this.activatedRoute.snapshot.queryParamMap.get('data');
        const jsonObject = JSON.parse(jsonString);
        console.log(jsonObject);
        const localStorageWardId = localStorage.getItem('ward_id');
        if (jsonObject != null || jsonObject != undefined) {
            this.queryParamsData = jsonObject;
        } else {
            this.queryParamsData = localStorageWardId;
        }
        this.dataWard = { id: this.queryParamsData };
        console.log(this.queryParamsData);

        this.wardName = this.activatedRoute.snapshot.queryParamMap.get('wardName');
        
    }

    async ngOnInit() {
        // this.user_login_name  =  this.userProfileService.user_login_name;
        this.user_login_name = sessionStorage.getItem('userLoginName');

        console.log(this.user_login_name);
        this.getWard();
        this.getDoctor();
    }
    /////////////เมธอด หลัก///////////ที่ต้องมี///////////////
    hideSpinner() {
        setTimeout(() => {
            this.spinner.hide();
        }, 1000);
    }

    async handleOk() {
        this.isOkLoading = true;
        setTimeout(async () => {
            console.log(this.selectedValue);
            let data: any = {
                doctor_id: this.selectedValue,
            };
            const response: AxiosResponse =
                await this.patientListService.changeDoctor(data, this.admitId);
            this.admitId = null;
            this.selectedValue = null;
            this.isVisible = false;
            this.isOkLoading = false;
            this.getWard();
        }, 3000);
    }

    handleCancel(): void {
        this.admitId = null;
        this.selectedValue = null;
        this.isVisible = false;
    }
    onGlobalFilter(table: Table, event: Event) {
        table.filterGlobal(
            (event.target as HTMLInputElement).value,
            'contains'
        );
    }
    displayFormAdd() {
        console.log('displayFormAdd');
    }
    displayFormEdit(data: any) {
        // this.isAdd = false;
        // this.isEdit = true;
        // this.hospitalIdEdit = data.hospital_id;
        // this.hospitalCodeEdit = data.hospital_code;
        // this.hospitalNameEdit = data.hospital_name;
        // this.hospitalActiveEdit = data.is_active;
        // this.displayForm = true;
    }
    async getDoctor() {
        console.log('getDoctor');
        try {
            const response: AxiosResponse =
                await this.lookupService.getDoctor();
            const responseData: any = response.data;
            const data: any = responseData.data;
            this.doctors = data;
            console.log('Doctor : ', this.doctors);
        } catch (error: any) {
            console.log(error);
            this.messageService.add({
                severity: 'dager',
                summary: 'เกิดข้อผิดพลาด #',
                detail: 'กรุณาติดต่อผู้ดูแลระบบ...' + error,
            });
        }
    }

    doSearch() {
        this.getAdmitActive();
    }
    logOut() {
        sessionStorage.setItem('token', '');
        return this.router.navigate(['/login']);
    }

    onPageIndexChange(pageIndex: any) {
        this.offset = pageIndex === 1 ? 0 : (pageIndex - 1) * this.pageSize;

        this.getAdmitActive();
    }

    onPageSizeChange(pageSize: any) {
        this.pageSize = pageSize;
        this.pageIndex = 1;

        this.offset = 0;

        this.getAdmitActive();
    }

    refresh() {
        this.query = '';
        this.pageIndex = 1;
        this.offset = 0;
        this.getAdmitActive();
    }

    async onSelectWard(event: any) {
        console.log(event);

        this.dataWard = event.value;
        this.getAdmitActive();
        this.wardName = event.value.name;
    }

    async getAdmitActive() {
        console.log('getAdmitActive');

        this.spinner.show();

        try {
            const _limit = this.pageSize;
            const _offset = this.offset;
            const response = await this.patientListService.getAdmitActive(
                this.dataWard.id,
                this.query,
                _limit,
                _offset
            );
            const data: any = response.data;
            console.log(data);

            this.total = data.total || 1;
            this.dataSet = data.data.map((v: any) => {
                const date = v.admit_date
                    ? DateTime.fromISO(v.admit_date)
                        .setLocale('th')
                        .toLocaleString(DateTime.DATE_MED)
                    : '';
                v.admit_date = date;
                const time = v.admit_time
                    ? DateTime.fromFormat(v.admit_time, 'HH:mm:ss').toFormat(
                        'HH:mm'
                    )
                    : '';
                v.admit_time = time;
                return v;
            });
        } catch (error: any) {
            this.messageService.add({
                severity: 'dager',
                summary: 'เกิดข้อผิดพลาด #',
                detail: 'กรุณาติดต่อผู้ดูแลระบบ...' + error,
            });
        }
    }

    async getWard() {
        console.log('getWard');
        try {
            const response: AxiosResponse = await this.lookupService.getWard();
            const responseData: any = response.data;
            const data: any = responseData.data;
            this.wards = data;
            console.log(this.wards);

            if (!_.isEmpty(data)) {
                const selectedWard: any = data[0];
                this.dataWard = selectedWard;
                this.wardName = selectedWard.name;
            }
            await this.getAdmitActive();
        } catch (error: any) {
            console.log(error);
        }
    }

    async getWardName(wardId: any) {
        const rsWardName = await this.lookupService.getWardName(wardId);
        console.log(rsWardName.data.data[0]);
        return rsWardName.data.data[0];
    }

    changeWard(data: any) {
        console.log(data);
        let dk: any = { admit_id: data };
        let jsonString = JSON.stringify(dk);
        console.log(jsonString);
        this.router.navigate(['/nurse/patient-list/change-ward'], {
            queryParams: { data: jsonString },
        });
    }
    async nurseNote(data: any) {
        // console.log(data);
        // let dk: any = { data };
        // console.log('dk : ', dk);

        // let jsonString = JSON.stringify(dk);
        // console.log(jsonString);
        sessionStorage.setItem('itemPatientInfo', JSON.stringify(data));

        console.log('itemPatientInfo data : ', data);

        await this.router.navigate(['/nurse/patient-list/nurse-note']); //, { queryParams: { data: jsonString } });
    }

    async changeDoctor(data: any) {
        console.log(data);
        this.admitId = await data.id;
        this.selectedValue = await data.doctor_id;
        console.log(this.selectedValue);

        this.isVisible = true;
    }

    async deleteAdmi7t(event: Event) {
        this.confirmationService.confirm({
            target: event.target as EventTarget,
            message: 'Are you sure that you want to proceed?',
            header: 'Confirmation',
            icon: 'pi pi-exclamation-triangle',
            acceptIcon: 'none',
            rejectIcon: 'none',
            rejectButtonStyleClass: 'p-button-text',
            accept: async () => {
                let data: any = {
                    is_active: false,
                };
                const response: AxiosResponse =
                    await this.patientListService.changeDoctor(data, 'id');
                this.getWard();
                // this.messageService.add({ severity: 'info', summary: 'Confirmed', detail: 'You have accepted' });
            },
            reject: () => {
                this.messageService.add({
                    severity: 'error',
                    summary: 'Rejected',
                    detail: 'You have rejected',
                    life: 3000,
                });
            },
        });
    }
    // confirm() {
    //     this.confirmationService.confirm({
    //         header: 'Confirmation',
    //         message: 'Please confirm to proceed moving forward.',
    //         acceptIcon: 'pi pi-check mr-2',
    //         rejectIcon: 'pi pi-times mr-2',
    //         rejectButtonStyleClass: 'p-button-sm',
    //         acceptButtonStyleClass: 'p-button-outlined p-button-sm',
    //         accept: () => {
    //             this.messageService.add({ severity: 'info', summary: 'Confirmed', detail: 'You have accepted', life: 3000 });
    //         },
    //         reject: () => {
    //             this.messageService.add({ severity: 'error', summary: 'Rejected', detail: 'You have rejected', life: 3000 });
    //         }
    //     });
    // }
    confirm() {
        this.confirmationService.confirm({
            header: 'Are you sure?',
            message: 'Please confirm to proceed.',
            accept: () => {
                this.messageService.add({ severity: 'info', summary: 'Confirmed', detail: 'You have accepted', life: 3000 });
            },
            reject: () => {
                this.messageService.add({ severity: 'error', summary: 'Rejected', detail: 'You have rejected', life: 3000 });
            }
        });
    }
    async deleteAdmit(event: Event) {
        console.log(event);
        let jsonEvent:any = event;
        this.confirmationService.confirm({
            target: event.target as EventTarget,
            message: 'Do you want to delete this record?',
            header: 'Delete Confirmation',
            icon: 'pi pi-info-circle',
            acceptButtonStyleClass:"p-button-danger p-button-text",
            rejectButtonStyleClass:"p-button-text p-button-text",
            acceptIcon:"none",
            rejectIcon:"none",

            accept: async () => {
                // this.messageService.add({ severity: 'info', summary: 'Confirmed', detail: 'Record deleted' });
                let data: any = {
                    is_active: false,
                   
                  };
                const response: AxiosResponse = await this.patientListService.changeDoctor( data, jsonEvent.id );
                  this.getWard();
            },
            reject: () => {
                this.messageService.add({ severity: 'error', summary: 'Rejected', detail: 'ยกเลิกกระบวนการ' });
            }
        });
    }
    async deleteAdmitn(datas:any) {
        console.log(datas);
        this.confirmationService.confirm({
            // target: event.target as EventTarget,
            message: 'คำเดือน คุณต้องการลบ จริงหรือไม่ ?',
            icon: 'pi pi-info-circle',
            acceptButtonStyleClass: 'p-button-danger p-button-sm',
            accept: () => {
                // this.messageService.add({ severity: 'info', summary: 'Confirmed', detail: 'Record deleted', life: 3000 });
                let data: any = {
                    is_active: false,
                  };
                // const response: AxiosResponse = await this.patientListService.changeDoctor( data, 'id' );
                //   this.getWard();
            },
            reject: () => {
                this.messageService.add({ severity: 'error', summary: 'Rejected', detail: 'You have rejected', life: 3000 });
            }
        });
    }
    navigateWaitingAdmit(data:any){
        console.log(data) ;
        let jsonString = JSON.stringify(data);
        console.log(jsonString);   
        this.router.navigate(['/waiting-admit'], { queryParams: { data: jsonString } });
    
      }
      navigateChangeWard(data:any){
        console.log(data) ;
        let jsonString = JSON.stringify(data);
        console.log(jsonString);   
        this.router.navigate(['/chang-ward'], { queryParams: { data: jsonString } });
    
      }
}
