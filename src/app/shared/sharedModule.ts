import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import {ThaiAgePipe} from '../pipes/to-thai-age.pipe';


@NgModule({
  declarations: [ ThaiAgePipe],
  imports: [
    CommonModule,
  ],
  // exports is required so you can access your component/pipe in other modules
  exports: [ThaiAgePipe]
})
export class SharedModule{}